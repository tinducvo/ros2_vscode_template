#!/bin/bash
set -e # terminal raises exceptions

# update Ubuntu
if [ "${EUID:-$(id -u)}" -eq 0 ]
then
    apt update -y
    apt upgrade -y
    apt autoremove -y
else
    echo "Not upgrading apt packages b/c not running as root"
fi