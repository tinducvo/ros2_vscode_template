#!/bin/bash
set -e # terminal raises exceptions

# build workspace packages
# with Python src/install symbolic,
# optimized debuggable binary / output compile_commands.json output,
# and throw (all, extra, c/pp standard) exceptions
BUILD_TYPE=RelWithDebInfo
colcon build \
   --symlink-install \
   --cmake-args "-DCMAKE_BUILD_TYPE=$BUILD_TYPE" "-DCMAKE_EXPORT_COMPILE_COMMANDS=On" \
   -Wall -Wextra -Wpedantic
