#!/bin/bash
set -e # terminal raises exceptions

if [ "${EUID:-$(id -u)}" -ne 0 ]
then
    # fix rosdep permissions
    rosdep fix-permissions

    # update rosdep sources
    rosdep update

    # install dependencies for packages in src
    # with priority to workspace src
    # and without prompts
    rosdep install \
        --from-paths src \
        --ignore-src \
        -y
else
    echo "Not upgrading rosdep packages b/c running as root"
fi