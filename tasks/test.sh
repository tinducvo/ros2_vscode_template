#!/bin/bash
set -e # terminal raises exceptions

colcon test # run tests
colcon test-result # display result of last run
