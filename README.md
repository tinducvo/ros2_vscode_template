# ROS2 VSCode Template

## Original Work
This template expands on [Allison Thackston's VSCode ROS2 Template](https://github.com/athackst/vscode_ros2_workspace) for full-featured ROS2 development.

[Supplement her notes](https://www.allisonthackston.com/articles/vscode_docker_ros2.html) with mine and inevitably you'll have questions, but I can improve the instructions if there is a demand.

---

## Requirements
### Soft
- Ubuntu Linux remote host
   - Docker bind mounting is used, which has worse performance than volume mounting on Windows and Mac
   - But negligible performance difference on Linux

---

## Installation
### Basic
1. On the local host
   1. Install Git and VSCode
   2. Connect to the remote host and added its public key to authorized_keys
   3. Install the recommended local extensions from ./local/settings.json
   3. Merge remote.SSH.defaultExtensions from ./local/settings.json into your user-scope settings.json
   4. Open your command palette and configure your remote host with "Remote-SSH: Open SSH Configuration File"
2. Connect to the remote host with the VSCode remote explorer extension
   1. Clone this repository
   2. Install docker and docker-compose from apt, not snap
   3. Open the command palette and run "Dev Containers: Open Container in Folder" on the workspace to start developing!

---

### Optional
#### ROS2 Native CLI and GUI
To use ros2 native commands
1. Install mamba
2. Copy ./local/ros2_humble.yml to the local host
3. Use the commented create command to create your ros2_humble environment
4. Use "mamba activate ros2_humble" to activate your environment
5. Use any ros2 native commands you need

#### VSCode Shortcut(s)
To easily access your remote VSCode, add a ROS2 VSCode bash shortcut to your .bashrc:
```
function code-ros2 {
  code $@ --user-data-dir={user_data_path} --extensions-dir={extensions_path}
}
```
Extend that to create a ROS2 Remote VSCode bash shortcut:
```
function code-ros2-remote {
  code-ros2 $@ --remote ssh-remote+{ssh_name}
}
```
And extend that to create a shortcut to any ROS2 Remote VSCode Workspace:

```
function code-ros2-{remote_workspace_name} {
  code-ros2-remote {remote_workspace_path} $@
}
```

#### Git Credential Manager
VSCode is able to forward credentials to the remote host and its dev containers with git-credential-manager
1. Install git-credential-manager with apt
2. Configure git to use git-credential-manager: credential.helper=manager
3. Configure git to use your desired backend: credential.credentialstore={backend}
   - Plaintext is security doesn't matter
   - Linux
      - gpg for headless
         - Add to .bashrc: export GPG_TTY=$(tty)
      - secretservice to avoid handling gpg
   - Windows
      - wincredman for non-ssh and easy gui
      - dpapi for ssh
   - Mac: keychain only

#### Docker Credential Manager
Log onto your docker image registry for increased limits or private access. I
1. Download the docker-credential-manager binary and place it on your path
2. Configure your ~/.docker/config.json to use your desired backend: "credsStore": "{backend}"
   - Linux
      - pass for headless
      - secretservice to avoid handling gpg
   - Windows: wincred only
   - Mac: osxkeychain only

---

## Initial Modifications from Original Work
Modifications are in the commit notes, but in short:
- General
   - Applied MIT license
   - Added comments to all configuration files
- Host ROS2 CLI
   - Added ROS2 Conda Environment, so the ROS2 native CLI and GUI tools could be used on non-tier 1 machines
      - Gazebo throws a rendering error on launch, so not included
   - Added settings.json
      - With commented extensions to install on the local host
      - And remote.SSH.defaultExtensions to be merged with the VSCode user configuration, so Docker is installed on the remote host      
- VSCode Configuration
   - Dev Container Configuration
      - Based on OSRF ROS2 image for longevity
      - Exposed UDP ports
      - Added explicit workspace bind mount for easy modification
   - Workspace Configuration
      - Settings
         - Disabled cSpell
         - Enabled flake8 linting
            - With same configuration as ament_flake8
            - And /opt/ros/humble/local/lib/python3.10/dist-packages added to extraPaths
      - Tasks
         - Switched from setup to apt_upgrade, rosdep_upgrade, vcs_export, and vcs_import
         - Reorganized and renamed tasks
      - C/C++
         - Switched to 2011 version of C
- Dev Container Contents
   - Added folder for tasks